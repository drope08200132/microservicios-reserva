package com.example.demo.reserva.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.reserva.models.Habitacion;

@Repository
public interface HabitacionRepository extends CrudRepository<Habitacion, Long> {

	@Query("select h from Habitacion h join fetch h.estado e where upper(e.nombre) like upper(concat('%',?1,'%'))")
	public Iterable<Habitacion> findHabitacionesByStatus(String status);
	
	@Query("select h from Habitacion h join fetch h.tipoHab t where upper(t.nombre) like upper(concat('%',?1,'%'))")
	public Iterable<Habitacion> findHabitacionesByTipo(String tipo);
}
