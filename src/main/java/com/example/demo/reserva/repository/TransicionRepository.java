package com.example.demo.reserva.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.reserva.models.Transicion;

public interface TransicionRepository extends CrudRepository<Transicion, Long>{

}
