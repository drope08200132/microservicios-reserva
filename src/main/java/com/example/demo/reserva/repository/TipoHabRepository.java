package com.example.demo.reserva.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.reserva.models.TipoHab;
@Repository
public interface TipoHabRepository extends CrudRepository<TipoHab, Long>{

}
