package com.example.demo.reserva.service;

import java.util.Optional;

import com.example.demo.reserva.models.Habitacion;
import com.example.demo.reserva.models.TipoHab;
import com.example.demo.reserva.models.Transicion;


public interface HabitacionService {
	public Iterable<Habitacion> findAll();
	public Optional<Habitacion> findById(Long id);
	public Habitacion save(Habitacion alumno);
	public void deleteById(Long id);
	public Iterable<Habitacion> findHabitacionesByStatus(String status);
	public Iterable<Habitacion> findHabitacionesByTipo(String tipo);
	public Optional<Transicion> findByIdEstado(Long idEstado);
	public Optional<TipoHab> findTipoHabById(Long id);
	
}
