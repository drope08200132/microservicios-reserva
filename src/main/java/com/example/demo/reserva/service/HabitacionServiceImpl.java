package com.example.demo.reserva.service;

import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.ReadOnlyProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.reserva.models.Habitacion;
import com.example.demo.reserva.models.TipoHab;
import com.example.demo.reserva.models.Transicion;
import com.example.demo.reserva.repository.HabitacionRepository;
import com.example.demo.reserva.repository.TipoHabRepository;
import com.example.demo.reserva.repository.TransicionRepository;



@Service
public class HabitacionServiceImpl implements HabitacionService  {

	@Autowired
	private HabitacionRepository repository ;
	@Autowired
	private TransicionRepository transicionRepository;
	@Autowired
	private TipoHabRepository tipoHabRepository;
	
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Habitacion> findAll() {
		
		return repository.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Optional<Habitacion> findById(Long id) {
		
		return repository.findById(id);
	}

	@Override
	@Transactional
	public Habitacion save(Habitacion entity) {
		
		return repository.save(entity);
	}

	@Override
	@Transactional(readOnly = true)
	public void deleteById(Long id) {
		repository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Iterable<Habitacion> findHabitacionesByStatus(String status) {
		
		return repository.findHabitacionesByStatus(status);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Iterable<Habitacion> findHabitacionesByTipo(String tipo) {
		
		return repository.findHabitacionesByTipo(tipo);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Transicion> findByIdEstado(Long idEstado) {
		// TODO Auto-generated method stub
		return transicionRepository.findById(idEstado);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<TipoHab> findTipoHabById(Long id) {
		// TODO Auto-generated method stub
		return tipoHabRepository.findById(id);
	}
	
	
}
