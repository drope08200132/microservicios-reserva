package com.example.demo.reserva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviciosReservaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviciosReservaApplication.class, args);
	}

}
