package com.example.demo.reserva.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transiciones")
public class Transicion {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private  Long id;
	@Column(name = "id_estado")
	private Long idEstado;
	@Column(name = "id_transiciones")
	private String idTransiciones;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}
	public String getIdTransiciones() {
		return idTransiciones;
	}
	public void setIdTransiciones(String idTransiciones) {
		this.idTransiciones = idTransiciones;
	}
	
	

	
	
}
