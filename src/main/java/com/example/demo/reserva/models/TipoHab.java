package com.example.demo.reserva.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "tipo_habitaciones")
public class TipoHab {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotEmpty
	private String nombre;
	@Column(name = "cupo_limite")
	@NotEmpty
	private Long cupoLim;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Long getCupoLim() {
		return cupoLim;
	}
	public void setCupoLim(Long cupoLim) {
		this.cupoLim = cupoLim;
	}
	
	
	
	

}
