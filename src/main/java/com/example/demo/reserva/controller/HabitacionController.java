package com.example.demo.reserva.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;

import com.example.demo.reserva.models.Habitacion;
import com.example.demo.reserva.models.TipoHab;
import com.example.demo.reserva.models.Transicion;
import com.example.demo.reserva.service.HabitacionService;


@RestController
@RequestMapping("/api/v1/")
public class HabitacionController {

	private Boolean  coincidencia=false;
	
	@Autowired
	protected HabitacionService service;
	
	//?: indica que puede recibir cualquier tipo de objeto
	@GetMapping("/habitaciones")
	public ResponseEntity<?> listar(){
		
		return ResponseEntity.ok().body(service.findAll());
		
	}
	
	@GetMapping("/habitaciones/estado/{status}")
	public ResponseEntity<?> verPorEstado(@PathVariable String status){
		return ResponseEntity.ok(service.findHabitacionesByStatus(status));
		
	}
	
	@GetMapping("/habitaciones/tipo/{tipo}")
	public ResponseEntity<?> verPorTipo(@PathVariable String tipo){
		return ResponseEntity.ok(service.findHabitacionesByTipo(tipo));
		
	}

	
	@GetMapping("/habitaciones/{id}")
	public ResponseEntity<?> ver(@PathVariable Long id){
		Optional<Habitacion> o = service.findById(id);
		if (!o.isPresent()) {
			return ResponseEntity.notFound().build();
		} 
		return ResponseEntity.ok(o.get());
		
	}
	
	
	
	@PostMapping("/habitaciones")
	public ResponseEntity<?> crear(@Valid @RequestBody Habitacion entity, BindingResult result){
		if(result.hasErrors()) {
			return this.validar(result);
		}
		//al crear una habitacion solo se puede crear en estado libre (1)
		if(entity.getEstado().getId() !=1 ) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("solo se puede crear una habitacion con el estado libre (id= 1)");
		}
		
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(entity));
		
	}
	
	
	
	@DeleteMapping("/habitaciones/{id}")
	public ResponseEntity<?> eliminar(@PathVariable Long id){
		service.deleteById(id);
		return ResponseEntity.noContent().build();
		
	}
	
	
	protected ResponseEntity<?> validar(BindingResult result){
		Map<String,Object> errores = new HashMap<>();
		
		result.getFieldErrors().forEach(err -> {
			errores.put(err.getField(),"el campo "+ err.getField()+" "+ err.getDefaultMessage());
		});
		return ResponseEntity.badRequest().body(errores);
	}
	
	@PutMapping("/habitaciones/transiciones/{id}")
	public ResponseEntity<?> transicion(@Valid @RequestBody Habitacion habitacion, BindingResult result,@PathVariable Long id){
		if(result.hasErrors()) {
			return this.validar(result);
		}
		Optional<Habitacion> hab = service.findById(id);
		if(hab.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		Habitacion h = hab.get();
		Optional<Transicion> transicion = service.findByIdEstado(h.getEstado().getId());
		String[] transicionesPermitidas= transicion.get().getIdTransiciones().split(",");
		List<String> transPermitidas = Arrays.asList(transicionesPermitidas);
		
		
		transPermitidas.stream().forEach(idt -> {
			if(idt.equals(habitacion.getEstado().getId().toString())) {
				Boolean c=true;
				coincidencia=c;
			}
		});
		
		if(!coincidencia) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("error : incorrecto valor de estado ingresado");
		}
		
		//si el estado de la transicion es ocupado o limpieza entonces debe tener cliente diferente a null
		if(habitacion.getEstado().getId()==2 || habitacion.getEstado().getId()==4) {
			if(habitacion.getCliente()==null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("error : debe tener un id cliente diferente a null");
			}
		}
		//si el estado de la transicion es LIBRE entonces debe tener cliente IGUAL a null
		if(habitacion.getEstado().getId()==1) {
			if(habitacion.getCliente()!=null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("error : debe tener un id cliente igual a null");
			}
		}
		
		Optional<TipoHab>tipohab=service.findTipoHabById(habitacion.getTipoHab().getId());
		if(!tipohab.isPresent()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("error : id de tipo de habitacion no existe");
			
		}
		
		
		if(!(habitacion.getCliente().getNumOcupantes() <= tipohab.get().getCupoLim())  ) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("error : numero de ocupantes sobrepasa el cupo maximo de la habitacion");
			
		}
		
		h.setEstado(habitacion.getEstado());
		h.setCliente(habitacion.getCliente());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(service.save(h));
	}
	
	
}
